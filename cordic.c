#include <stdio.h>
#include <math.h>
#define PI 3.14159265358979323846
int CORDIC(int* angles, int K, int steps, int target);


int main()
{
    double temp_angle;
    double temp_K;
    double old_K=1;
    double temp_partial;
    double t;
    double angle_round;
    int temp;
    int steps = 16;
    int angles[steps];
    int target;
    int K=1;
    int i;
    int SIN_216;
    int COS_216;
    int * angles_ptr;
    
    
    //Generate constant values delta_ai and temp_K
    //Depends on max number of steps used
    //Values should stay below +- 2^18 so 32 bit int should be ok 
    for (i = 0; i < steps; i++)
    {
        temp = 1 << i; //Calculate 2^-i
        t = 1.0/temp; //Don't use fixed point + <<'s yet
        temp_angle = atan(t);
        temp_angle = temp_angle * (1 << 16); //convert to 2.16
        angles[i]= (int) round(temp_angle); //round
        printf("angle %d is %d\n",i+1,angles[i]);
        
        temp = 1 << 2*i; //Calculate 2^-2i
        t = 1.0/temp;
        if (i >=16) //avoid overflow
        {
            t = 0.0;
        }
        temp_partial = sqrt(1 + t);
        temp_K = (1.0/temp_partial) * old_K;
        old_K = temp_K;
    }
    K = (int) (temp_K * (1 << 16) );
    printf("K is %d\n", K);
    angles_ptr = &angles[0];
    
    //Valid angles from pi/ste2 too -pi/2
    //t = (PI/2.7) * (1 << 16);
	t = 97436; //should be max diff of 11
    target = (int) round(t);
    angle_round =  (double) target/(1<<16);
    printf("angle round is %2f\n", angle_round);
    //target = target - 1; //rounds up and its out of bounds pi/2 only
    printf("target 2.16 angle is %d\n", target);
    temp = CORDIC(angles_ptr, K, steps, target );
    //printf("Result is %d\n", result);
    
    SIN_216 = (int) round ( (sin(angle_round) * (1 << 16)) );
    COS_216 = (int) round ( (cos(angle_round) * (1 << 16)) );
    printf("cos is %d and sin is %d\n", COS_216, SIN_216);
    return 0;
}

int CORDIC(int* angles, int K, int steps, int target)
{
    int i;
    int angle;
    int C;
    int C_old;
    int S;
    int S_old; 
    
    //Main loop for CORDIC sin/cos calculation
    //Requires 2.16 ints: a[steps], K, steps, target
    //2^-i multiplier = right shift by i+1 
    //TODO multiply by K to get S_old C_old original skip i=0
	printf("Starting CORDIC\n");
	//n cycle version, init with [K,0]
	C = K;
	S = 0;
	if ( target >= 0 ) 
		{ 
		    angle = angles[0];
			C = K;
			S = K;
		}
		else 
		{ // <0. 
		
		    angle = -angles[0];
			C = K;
			S = -K;
		}
    for ( i = 1; i < steps; i++ )
	 {
		C_old = C; S_old = S;
		if ( angle <= target ) 
		{ // undershoot. 
			angle += angles[i];
			C = C_old - (S_old >> i);
			S = S_old + (C_old >> i);
		}
		else 
		{ // overshoot.
			angle -= angles[i];
			C = C_old + (S_old >> i);
			S = S_old - (C_old >> i);
		}
	}
	printf("Cos is %d and Sin is %d\n", C, S);
    return 1;
}
